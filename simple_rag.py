import torch
from transformers import AutoTokenizer, AutoModel



class SentenceSimilar:
    def mean_pooling(self, token_embeddings, mask):
        token_embeddings = token_embeddings.masked_fill(~mask[..., None].bool(), 0.)
        sentence_embeddings = token_embeddings.sum(dim=1) / mask.sum(dim=1)[..., None]
        return sentence_embeddings


    def score_pairs(self, sen):
        model = AutoModel.from_pretrained("CarperAI/carptriever-1", add_pooling_layer=False)
        tokenizer = AutoTokenizer.from_pretrained("CarperAI/carptriever-1")

        # Apply tokenizer
        inputs = tokenizer(sen, padding=True, truncation=True, return_tensors='pt')

        # Encode sentences
        outputs = model(**inputs)
        embeddings = self.mean_pooling(outputs[0], inputs['attention_mask'])

        # Compute dot-product scores between the query and sentence embeddings
        query_embedding, sentence_embeddings = embeddings[0], embeddings[1:]
        scores = (query_embedding @ sentence_embeddings.transpose(0, 1)).cpu().tolist()

        return(sorted(zip(sen[1:], scores), reverse=True))




    def similar(self, sentences, query):
        sentences.append(query)
        sentence_score_pairs = self.score_pairs(list(reversed(sentences)))
        sec = [sentence_score_pairs[d][1] for d in range(len(sentence_score_pairs))]
        sec = sentence_score_pairs[sec.index(max(sec))][0]
        return(sec)





def main(text_input):
    data = ["Intro of myself?", "What is my experience?", "Why would you be suitable for the role?"]
    Similar = SentenceSimilar()
    sim = Similar.similar(data, text_input)
    data = {"Intro of myself?": "I'm Machine Learning engineer by trade been in the industry for just over a decade (now). Together, with the team we galvinized the indsutry through projects and over 100k impressions", "What is my experience?": "Worked closely with a lot of engineers over the years", "Why would you be suitable for the role?": "This is perfect role for me as it's my passion to stay secure and safe...."}
    print(data[sim])





